package edu.im.abe;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import android.os.Environment;

public class VideoStreamer {
	
	private Socket clientSocket = new Socket();
	private OutputStream bos ;
	private InputStream  ios ;
	
	public void connect(String hostname, int port) throws IOException{
		
		clientSocket.connect(new InetSocketAddress(hostname, port));
		 bos = clientSocket.getOutputStream();
		 ios = clientSocket.getInputStream();
		
	}
	public void send(VideoFrame videoFrame) throws IOException{
		byte buff[] = new byte[256];
		
		//File sdCard = Environment.getExternalStorageDirectory();
		//File file = new File(sdCard, "test.jpg");
		//FileOutputStream f = new FileOutputStream(file);
		
		ByteBuffer bb = ByteBuffer.allocate(16);
		InputStream is = videoFrame.getInputStream();
		bb.order(ByteOrder.BIG_ENDIAN);
		bb.putInt(1);
		bb.putInt(is.available());
		bb.putInt(10);
		bb.putInt(20);
		bb.rewind();
		bb.get(buff, 0, 16);
		int start = 16;
		
		int read;
		int isav = is.available();
		int count =0;
		while (is.available() > 0){
			read = is.read(buff, start, 256 -start);
			count+=read;
			bos.write(buff,0,read+start);
			//f.write(buff, 0, read+start);
			start = 0;
		}
		
		bos.flush();
		is.close();
		int ack;
		// ack = ios.read();
	}
	
	public void close() throws IOException{
		clientSocket.close();
	}

}
