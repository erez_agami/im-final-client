package edu.im.abe;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.os.Bundle;
import android.view.Display;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MainActivity extends Activity 
 implements View.OnTouchListener, CameraView.CameraReadyCallback, OverlayView.UpdateDoneCallback{
	
	private static final String TAG = "MainActivity";
	
	private static final String SERVER_URL="192.168.43.247";
	private static final int SERVER_PORT = 5000;
	
	
	private byte perFrame[] = new byte[1024 * 1024 * 8]; 
	VideoFrame targetFrame = new VideoFrame(1024*1024*8);
	private Button btnExit;

	private boolean inProcessing = false;
	private CameraView cameraView_;
	private OverlayView overlayView_;
	private VideoStreamer videoStreamer = new VideoStreamer();
	private Thread networkThread;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window win = getWindow();
		win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		setContentView(R.layout.main);
		initCamera();
		addListeners();	
		
		networkThread = new Thread(networkRunable);
		networkThread.start();
	}

	private void addListeners(){
		
		ImageView image = (ImageView) findViewById(R.id.probeView1);
		image.setOnClickListener(new OnClickListener() {
 
			@Override
			public void onClick(View arg0) {
				//TODO:: Send probe action to server
			}
 
		});
		
		image = (ImageView) findViewById(R.id.probeView2);
				
				image.setOnClickListener(new OnClickListener() {
		 
					@Override
					public void onClick(View arg0) {
						//TODO:: Send probe action to server
						
					}
 
		});
				
		image = (ImageView) findViewById(R.id.modelView1);
					
					image.setOnClickListener(new OnClickListener() {
			 
						@Override
						public void onClick(View arg0) {
							//TODO:: Send model chosen action to server
						}
	 
			});
					
		image = (ImageView) findViewById(R.id.modelView2);
						
					image.setOnClickListener(new OnClickListener() {
			 
						@Override
						public void onClick(View arg0) {
							//TODO:: Send model chosen action to server
						}
	 
			});
	
	  
					
 
	}
	
	
	@Override
	public void onCameraReady() {
	      int wid = cameraView_.Width();
	      int hei = cameraView_.Height();
	      cameraView_.StopPreview();
	      cameraView_.setupCamera(wid, hei, previewCb_);
	      cameraView_.StartPreview();
		
	}

	@Override
	public void onUpdateDone() {
		// TODO Auto-generated method stub
		
	}

 @Override
    public void onPause(){  
        super.onPause();
        inProcessing = true;
        cameraView_.StopPreview(); 
        //cameraView_.Release();
    
        //System.exit(0);
        finish();
    }  
	 
	@Override
	public boolean onTouch(View arg0, MotionEvent arg1) {
		return false;
	}
	
	
	private void initCamera() {
        SurfaceView cameraSurface = (SurfaceView)findViewById(R.id.surface_camera);
        cameraView_ = new CameraView(cameraSurface);        
        cameraView_.setCameraReadyCallback(this);

        overlayView_ = (OverlayView)findViewById(R.id.surface_overlay);
        overlayView_.setOnTouchListener(this);
        overlayView_.setUpdateDoneCallback(this);
    }
	
	private OnClickListener exitAction = new OnClickListener() {
        @Override
        public void onClick(View v) {
            onPause();
        }   
    };
    
    private PreviewCallback previewCb_ = new PreviewCallback() {
        public void onPreviewFrame(byte[] frame, Camera c) {
            if ( !inProcessing ) {
                inProcessing = true;
           
                int picWidth = cameraView_.Width();
                int picHeight = cameraView_.Height(); 
                ByteBuffer bbuffer = ByteBuffer.wrap(frame); 
                bbuffer.get(perFrame, 0, picWidth*picHeight + picWidth*picHeight/2);

                inProcessing = false;
                
                
            }
        }
    };
    
    private VideoFrame getImage(){
    	
        int picWidth = cameraView_.Width();
        int picHeight = cameraView_.Height(); 
        if (picHeight <=0 || picHeight <= 0){
        	return null;
        }

        targetFrame.reset();
        YuvImage newImage = new YuvImage(perFrame, ImageFormat.NV21, picWidth, picHeight, null);

        try{
            newImage.compressToJpeg( new Rect(0,0,picWidth,picHeight), 30, targetFrame);
        } catch (Exception ex) {  
        } 
        return targetFrame;
    }
    Runnable networkRunable = new Runnable() {
		
		@Override
		public void run() {
			
			try {
				videoStreamer.connect(SERVER_URL, SERVER_PORT);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			
			while (true){
				try {
					Thread.sleep(300);
					VideoFrame frame = getImage();
					if (frame != null){
						videoStreamer.send(frame);
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		
	};
	
}
